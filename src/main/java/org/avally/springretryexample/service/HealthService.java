package org.avally.springretryexample.service;

import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author Dmitry Polishchuk
 */

@Service
public class HealthService {
    private RestTemplate restTemplate = new RestTemplate();

    private int count;

    public void clearCount() {
        count = 0;
    }

    @Retryable(maxAttempts = 2, value = RuntimeException.class, backoff = @Backoff(delay = 200, multiplier = 2))
    public String getHealth() {
        count++;
        return restTemplate.getForObject("http://localhost:8080/health", String.class) + " " + count;
    }

    @Recover
    public String recover() {
        return "not ok";
    }
}
