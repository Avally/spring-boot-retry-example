package org.avally.springretryexample.controller;

import org.avally.springretryexample.service.HealthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Dmitry Polishchuk
 */

@RestController
public class HealthCheckController {
    private final HealthService healthService;

    @Autowired
    public HealthCheckController(HealthService healthService) {
        this.healthService = healthService;
    }

    @GetMapping("/check")
    public String checkStatus() {
        healthService.clearCount();
        return healthService.getHealth();
    }
}
