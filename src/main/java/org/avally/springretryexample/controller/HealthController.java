package org.avally.springretryexample.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

/**
 * @author Dmitry Polishchuk
 */

@RestController
public class HealthController {

    @GetMapping("/health")
    public ResponseEntity<String> getStatus() {
        Random random = new Random();
        int randInt = random.nextInt(2);
        if (randInt == 1) {
            return new ResponseEntity<>("INTERNAL_SERVER_ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity<>("OK", HttpStatus.OK);
        }
    }
}
